package ru.sadkov.tm.model.enumerate;

public enum Role {
    USER,
    ADMIN;

    Role() {
    }
}
