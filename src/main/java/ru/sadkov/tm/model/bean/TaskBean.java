package ru.sadkov.tm.model.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.model.enumerate.Status;
import ru.sadkov.tm.service.ITaskService;

import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Named(value = "task")
@SessionScoped
@Getter
@Setter
@NoArgsConstructor
public class TaskBean {

    @Inject
    private ITaskService taskService;

    @Nullable
    String id;
    @Nullable
    String name;
    @Nullable
    String description;
    @Nullable
    Date dateCreate;
    @Nullable
    Date dateBegin;
    @Nullable
    Date dateEnd;
    @Nullable
    Status status;
    @Nullable
    Task currentTask;

    private void clear() {
        id = null;
        name = null;
        description = null;
        dateCreate = null;
        dateBegin = null;
        dateEnd = null;
        status = null;
        currentTask = null;
    }

    public List<Task> getTasks(@NotNull final String projectId) {
        return taskService.findAllByProjectId(projectId);
    }

    public String saveTask(@NotNull final TaskBean taskBean,@NotNull final String projectName) {
        taskService.saveTask(taskBean.name, projectName, taskBean.description);
        clear();
        return "/tasks.xhtml?faces-redirect=true";
    }

    public String showTask(@NotNull final String taskId) {
        currentTask = taskService.findOneById(taskId);
        return "/task.xhtml?faces-redirect=true";
    }

    public String delete(@NotNull final String taskId) {
        taskService.removeById(taskId);
        return "/tasks.xhtml?faces-redirect=true";
    }

    public String start(@NotNull final String taskName) {
        taskService.startTask(taskName);
        return "/tasks.xhtml?faces-redirect=true";
    }

    public String end(@NotNull final String taskName) {
        taskService.endTask(taskName);
        return "/tasks.xhtml?faces-redirect=true";
    }

    public String update(@NotNull final String taskId) {
        currentTask = taskService.findOneById(taskId);
        return "/tUpdate.xhtml?faces-redirect=true";
    }

    public String updateTask(@NotNull final TaskBean taskBean,@NotNull final String taskId) {
        taskService.update(taskId, taskBean.name, taskBean.description);
        clear();
        return "/tasks.xhtml?faces-redirect=true";
    }

}
