package ru.sadkov.tm.model.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.model.User;
import ru.sadkov.tm.model.enumerate.Status;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.IUserService;

import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Named(value = "project")
@SessionScoped
@Getter
@Setter
@NoArgsConstructor
public class ProjectBean {

    @Inject
    private IProjectService projectService;

    @Inject
    private IUserService userService;

    @Nullable
    List<Task> taskList;
    @Nullable
    String id;
    @Nullable
    String name;
    @Nullable
    String description;
    @Nullable
    Date dateCreate;
    @Nullable
    Date dateBegin;
    @Nullable
    Date dateEnd;
    @Nullable
    Status status;
    @Nullable
    String userId;
    @Nullable
    Project currentProject;

    private void clear() {
        taskList = null;
        id = null;
        name = null;
        description = null;
        dateCreate = null;
        dateBegin = null;
        dateEnd = null;
        status = null;
        userId = null;
        currentProject = null;
    }

    @NotNull
    public String saveProject(@NotNull final ProjectBean projectBean,@NotNull final String userId) {
        projectService.persist(userId, projectBean.name, projectBean.description);
        clear();
        return "/projects.xhtml?faces-redirect=true";
    }

    @NotNull
    public List<Project> getProjects() {
        @NotNull final String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return projectService.findProjectsByUser(username);
    }

    @NotNull
    public String showProject(@NotNull final String projectId) {
        currentProject = projectService.findOne(projectId);
        return "/project.xhtml?faces-redirect=true";
    }

    @NotNull
    public String delete(@NotNull final String projectId) {
        projectService.remove(projectId);
        return "/projects.xhtml?faces-redirect=true";
    }

    @NotNull
    public String start(@NotNull final String projectName) {
        projectService.startProject(projectName);
        return "/projects.xhtml?faces-redirect=true";
    }

    @NotNull
    public String end(@NotNull final String projectName) {
        projectService.endProject(projectName);
        return "/projects.xhtml?faces-redirect=true";
    }

    @NotNull
    public String update(@NotNull final String projectId) {
        currentProject = projectService.findOne(projectId);
        return "/pUpdate.xhtml?faces-redirect=true";
    }

    @NotNull
    public String updateProject(@NotNull final ProjectBean projectBean,@NotNull final String projectId) {
        projectService.update(projectId, projectBean.name, projectBean.description);
        clear();
        return "/projects.xhtml?faces-redirect=true";
    }

    @NotNull
    public String showTasks(@NotNull final String projectId) {
        currentProject = projectService.findOne(projectId);
        return "/tasks.xhtml?faces-redirect=true";
    }

    @NotNull
    public String createTask(@NotNull final String projectId) {
        currentProject = projectService.findOne(projectId);
        return "/taskCreate.xhtml?faces-redirect=true";
    }
}
