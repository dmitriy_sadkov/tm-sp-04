package ru.sadkov.tm.model.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.User;
import ru.sadkov.tm.model.enumerate.Role;
import ru.sadkov.tm.service.IUserService;

import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named(value = "user")
@SessionScoped
@Getter
@Setter
@NoArgsConstructor
public class UserBean {

    @Inject
    @NotNull
    private IUserService userService;

    @Nullable
    private User currentUser;
    @Nullable
    private String id;
    @Nullable
    private String login;
    @Nullable
    private String password;
    @Nullable
    private List<Project> projectList;
    @Nullable
    private Role role;

    private void clearFields() {
        id = null;
        login = null;
        password = null;
        projectList = null;
        role = null;
    }

    @NotNull
    public String addUser() {
        @NotNull final User user = userService.createUser(login, password);
        userService.userRegister(user);
        clearFields();
        return "/index.xhtml";
    }

    @NotNull
    public String createProject() {
        @NotNull final String username = SecurityContextHolder.getContext().getAuthentication().getName();
        currentUser = userService.findOneByLogin(username);
        return "/projectCreate.xhtml";
    }

    @NotNull
    public String logout() {
        return "/index.xhtml";
    }

    @NotNull
    public boolean isAuth() {
        return SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser");
    }

    @NotNull
    public String getName() {
        if (isAuth()) return "GUEST";
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}
